" Setup language -------------------------------------------------------  {{{
  " language en_US.UTF-8           " Solve some plugins incompatibilities
" }}}

" Setup dein {{{
if &compatible
  set nocompatible
endif
filetype off
if (!isdirectory(expand("$HOME/.config/nvim/dein/dein.vim")))
  call system(expand("mkdir -p $HOME/.config/nvim/dein"))
  call system(expand("git clone https://github.com/Shougo/dein.vim $HOME/.config/nvim/dein/dein.vim"))
endif


set runtimepath+=~/.config/nvim/dein/dein.vim/


if dein#load_state(expand('~/.config/nvim/dein'))
call dein#begin(expand('~/.config/nvim/dein'))
call dein#add('Shougo/vimproc', {'build' : 'make'})
call dein#add('Shougo/denite.nvim')
call dein#add('Shougo/neomru.vim')
call dein#add('haya14busa/dein-command.vim')

" Shougo Denite ---------------------------------------------------------- {{{
call dein#add('Shougo/denite.nvim')
" }}}

" Shougo deoplete ------------------------------------------------------- {{{
call dein#add('Shougo/deoplete.nvim')
call dein#add('Shougo/deol.nvim')
call dein#add('Shougo/neco-vim', {'on_ft': 'vim'})
call dein#add('Shougo/neoinclude.vim')
call dein#add('ujihisa/neco-look')
" }}}

" Aesthetics
" Themes
call dein#add('morhetz/gruvbox')
call dein#add('tomasr/molokai')
call dein#add('wesgibbs/vim-irblack')
call dein#add('frankier/neovim-colors-solarized-truecolor-only')
call dein#add('romainl/flattened')
call dein#add('nanotech/jellybeans.vim')
call dein#add('w0ng/vim-hybrid')
call dein#add('vim-airline/vim-airline')
call dein#add('vim-airline/vim-airline-themes')
call dein#add('mhinz/vim-startify')
call dein#add('vim-niji')

"" Tools
call dein#add('MattesGroeger/vim-bookmarks')
call dein#add('Raimondi/delimitMate')
call dein#add('mbbill/undotree')
call dein#add('rking/ag.vim')
call dein#add('easymotion/vim-easymotion')
call dein#add('majutsushi/tagbar')
call dein#add('ntpeters/vim-better-whitespace')
call dein#add('mru.vim')
call dein#add('ctrlpvim/ctrlp.vim')
call dein#add('tacahiroy/ctrlp-funky')
call dein#add('scrooloose/nerdtree')
call dein#add('terryma/vim-multiple-cursors')
call dein#add('dyng/ctrlsf.vim')
call dein#add('chrisbra/NrrwRgn')
call dein#add('blindFS/vim-taskwarrior')
call dein#add('Shougo/neoinclude.vim')
call dein#add('Shougo/neosnippet')
call dein#add('Shougo/neosnippet-snippets')
call dein#add('majutsushi/tagbar')
call dein#add('AndrewRadev/splitjoin.vim')
call dein#add('tpope/vim-obsession')

""" Git tools
call dein#add('tpope/vim-fugitive')
call dein#add('Kocha/vim-unite-tig')
call dein#add('airblade/vim-gitgutter')

"" Languages

"" Elixir
call dein#add('elixir-lang/vim-elixir')

"" Rust
call dein#add('rust-lang/rust.vim')
call dein#add('cespare/vim-toml')

"" Clojure
call dein#add('guns/vim-clojure-static')
call dein#add('kien/rainbow_parentheses.vim')
call dein#add('tpope/vim-fireplace')
call dein#add('vim-scripts/paredit.vim')
call dein#add('venantius/vim-cljfmt')
call dein#add('clojure-vim/async-clj-omni')

"" Markdown
call dein#add('tpope/vim-markdown')
call dein#add('suan/vim-instant-markdown')

"" html + css + javascript
call dein#add('alvan/vim-closetag')
call dein#add('pangloss/vim-javascript')
call dein#add('othree/javascript-libraries-syntax.vim')
call dein#add('hail2u/vim-css3-syntax')
call dein#add('othree/html5.vim')
call dein#add('gorodinskiy/vim-coloresque')
call dein#add('skammer/vim-css-color')
call dein#add('tpope/vim-haml')
call dein#add('mattn/emmet-vim')
call dein#add('groenewege/vim-less')
call dein#add('kchmck/vim-coffee-script')
call dein#add('elzr/vim-json')
call dein#add('digitaltoad/vim-jade')
call dein#add('mxw/vim-jsx')

"" AngularJS
call dein#add('burnettk/vim-angular')
call dein#add('matthewsimo/angular-vim-snippets')
call dein#add('curist/vim-angular-template')

" Mustache/handlebars
call dein#add('mustache/vim-mustache-handlebars')

"" others tools for langs
call dein#add('tpope/vim-commentary')
call dein#add('sheerun/vim-polyglot')
call dein#add('scrooloose/syntastic')
call dein#add('myint/syntastic-extras')
call dein#add('chrisbra/csv.vim')
call dein#add('tpope/vim-liquid')
call dein#add('tpope/vim-endwise')
call dein#add('tpope/vim-surround')
call dein#add('pearofducks/ansible-vim')
call dein#add('Matt-Stevens/vim-systemd-syntax')
call dein#add('tfnico/vim-gradle')
call dein#add('ekalinin/Dockerfile.vim')
call dein#add('kien/rainbow_parentheses.vim')

"" SnipMate
call dein#add('MarcWeber/vim-addon-mw-utils')
call dein#add('tomtom/tlib_vim')
call dein#add('garbas/vim-snipmate')
call dein#add('honza/vim-snippets')

if dein#check_install()
  call dein#install()
  let pluginsExist=1
endif

call dein#end()
call dein#save_state()
endif

filetype plugin indent on
" }}}

" Init ----------------------------------------------- {{{

set noexrc
set background=dark
set fenc=utf-8
set cpoptions=aABceFsmq
"             |||||||||
"             ||||||||+-- When joining lines, leave the cursor between joined lines
"             |||||||+-- When a new match is created (showmatch) pause for .5
"             ||||||+-- Set buffer options when entering the buffer
"             |||||+-- :write command updates current file name automatically add <CR> to the last line when using :@r
"             |||+-- Searching continues at the end of the match at the cursor position
"             ||+-- A backslash has no special meaning in mappings
"             |+-- :write updates alternative file name
"             +-- :read updates alternative file name
let g:skip_loading_mswin=1 " Just in case :)

" Newish
set history=9999 " big old history
set timeoutlen=300 " super low delay (works for me)
set formatoptions+=n " Recognize numbered lists
set formatlistpat=^\\s*\\(\\d\\\|[-*]\\)\\+[\\]:.)}\\t\ ]\\s* "and bullets, too
set viminfo+=! " Store upper-case registers in viminfo
set nomore " Short nomore

" }}}

" Settings ------------------------------------------- {{{

syntax enable
set termguicolors
set background=dark
let g:hybrid_custom_term_colors = 1
let g:hybrid_reduced_contrast = 1 " Remove this line if using the default palette."

color hybrid "gruvbox

let g:solarized_termcolors=256
let g:solarized_termtrans=0

set regexpengine=1

set encoding=utf-8
set fileencoding=utf-8
set fileencodings=utf-8
filetype indent on

set directory=/tmp/
set smarttab
set shiftwidth=2
set shiftround
set softtabstop=0
set expandtab

syntax on
set wmnu
set wildmode=list:longest,list:full
set list " Show invisible characters


set listchars=""
set listchars=tab:▸▸
set listchars+=trail:.
set listchars+=extends:>
set listchars+=precedes:<
set visualbell
set noerrorbells

set hidden                                                " don't unload buffer when switching away
set modeline                                              " allow per-file settings via modeline
set secure                                                " disable unsafe commands in local .vimrc files
set nobackup nowritebackup noswapfile autoread            " no backup or swap
set hlsearch incsearch ignorecase smartcase               " search
set wildmenu                                              " completion
set backspace=indent,eol,start                            " sane backspace
set clipboard=unnamedplus                                 " use the system clipboard for yank/put/delete
set mouse=a                                               " enable mouse for all modes settings
set nomousehide                                           " don't hide the mouse cursor while typing
set mousemodel=popup                                      " right-click pops up context menu
set ruler                                                 " show cursor position in status bar
set number                                                " show absolute line number of the current line
set relativenumber
set nofoldenable                                          " I fucking hate code folding
set scrolloff=10                                          " scroll the window so we can always see 10 lines around the cursor
set textwidth=80                                          " wrap at 80 characters like a valid human
set printoptions=paper:letter                             " use letter as the print output format
set winaltkeys=no                                         " turn off stupid fucking alt shortcuts
set laststatus=2                                          " always show status bar
set shell=/bin/sh
set bomb
set binary
set ttyfast
set fileformats=unix,dos,mac
set showcmd
set cursorline                                            " Highlight current line
set complete-=i                                           " set complete=.,w,b,u,t
set completeopt=menu,preview
set tags+=.git/tags
set nojoinspaces                                          " Don't add 2 spaces when using J
set autowriteall                                          " Write the contents of the file as frequent as possible


" }}}

" Autocommands --------------------------------------- {{{
"" Json file
au BufRead,BufNewFile *.json set filetype=json

"" markdown configuration
autocmd BufNewFile,BufReadPost *.md set filetype=markdown " for plugin understand all files with extension .md not only README.md

"" clojurescript configuration
autocmd BufRead,BufNewFile *.cljs setlocal filetype=clojure

"" XML file
au FileType xml setlocal equalprg=xmllint\ --format\ --recover\ -\ 2>/dev/null
" }}}

" Mappings ------------------------------------------- {{{
"" Map leader to ,
let mapleader=','
let maplocalleader= ' '

"" Tabs
nnoremap <Tab> gt
nnoremap <S-Tab> gT
nnoremap <silent> <S-t> :tabnew<CR>

"" Set working directory
nnoremap <leader>. :lcd %:p:h<CR>

"" Opens an edit command with the path of the currently edited file filled in
noremap <Leader>e :e <C-R>=expand("%:p:h") . "/" <CR>

"" Opens a tab edit command with the path of the currently edited file filled
noremap <Leader>te :tabe <C-R>=expand("%:p:h") . "/" <CR>

" Format entire file
nmap <leader>fef ggVG=

"" Copy and paste
noremap YY "+y<CR>
noremap P "+gP<CR>
noremap XX "+x<CR>


"" Buffer nav
noremap <leader>z :bp<CR>
noremap <leader>q :bp<CR>
noremap <leader>x :bn<CR>
noremap <leader>w :bn<CR>

"" Close buffer
noremap <leader>c :bd<CR>

"" Clean search (highlight)
nnoremap <silent> <leader><space> :noh<cr>

"" Key binding
"" NerdTree
map <C-e> :NERDTreeToggle<CR>:NERDTreeMirror<CR>
map <leader>e :NERDTreeFind<CR>
nmap <leader>nt :NERDTreeFind<CR>

"" LESS
nnoremap <Leader>m :w <BAR> !lessc % > %:t:r.css<CR><space>

"" JSON
nmap <leader>jt <Esc>:%!python -m json.tool<CR><Esc>:set filetype=json<CR>

"" Fix tab, change tab by spaces
nmap <leader>rt <Esc>:set softtabstop=0<CR><Esc>:set expandtab<CR><Esc>:set smarttab<CR><Esc>:set shiftwidth=2<CR>gg=G<Esc>:retab<CR>

"" HTML identation
nmap <leader>ht <Esc>:!tidy -mi -html -wrap 0 %<CR><Esc>:set filetype=html<CR>

"" Fugitive
nnoremap <silent> <leader>gs :Gstatus<CR>
nnoremap <silent> <leader>gd :Gdiff<CR>
nnoremap <silent> <leader>gc :Gcommit<CR>
nnoremap <silent> <leader>gb :Gblame<CR>
nnoremap <silent> <leader>gl :Glog<CR>
nnoremap <silent> <leader>gp :Git push<CR>
"" Mnemonic_i_nteractive
nnoremap <silent> <leader>gi :Git add -p %<CR>
nnoremap <silent> <leader>gg :SignifyToggle<CR>

"" Tagbar
nmap <silent> <F4> :TagbarToggle<CR>

"" RSpec
map <Leader>t :call RunCurrentSpecFile()<CR>
map <Leader>s :call RunNearestSpec()<CR>
map <Leader>l :call RunLastSpec()<CR>
map <Leader>a :call RunAllSpecs()<CR>

"" Ruby Refactoring
nnoremap <leader>rap  :RAddParameter<cr>
nnoremap <leader>rcpc :RConvertPostConditional<cr>
nnoremap <leader>rel  :RExtractLet<cr>
vnoremap <leader>rec  :RExtractConstant<cr>
vnoremap <leader>relv :RExtractLocalVariable<cr>
nnoremap <leader>rit  :RInlineTemp<cr>
vnoremap <leader>rrlv :RRenameLocalVariable<cr>
vnoremap <leader>rriv :RRenameInstanceVariable<cr>
vnoremap <leader>rem  :RExtractMethod<cr>

"" Unite tig
nnoremap <silent> <leader>ut :<C-u>Unite tig -no-split<CR>
nnoremap <silent> <leader>uta :<C-u>Unite tig -no-split -auto-preview<CR>

"" Undotree
nnoremap <silent> <leader>u :UndotreeToggle<CR>

"" CtrlPFunky
nnoremap <Leader>fu :CtrlPFunky<Cr>
" narrow the list down with a word under cursor
nnoremap <Leader>fU :execute 'CtrlPFunky ' . expand('<cword>')<Cr>

"" CtrlSF
nmap     <C-F>f <Plug>CtrlSFPrompt
vmap     <C-F>f <Plug>CtrlSFVwordPath
vmap     <C-F>F <Plug>CtrlSFVwordExec
nmap     <C-F>n <Plug>CtrlSFCwordPath
nmap     <C-F>p <Plug>CtrlSFPwordPath
nnoremap <C-F>o :CtrlSFOpen<CR>
nnoremap <C-F>t :CtrlSFToggle<CR>
inoremap <C-F>t <Esc>:CtrlSFToggle<CR>

" Plugin key-mappings.
imap <C-k>     <Plug>(neosnippet_expand_or_jump)
smap <C-k>     <Plug>(neosnippet_expand_or_jump)
xmap <C-k>     <Plug>(neosnippet_expand_target)

" SuperTab like snippets behavior.
imap <expr><TAB>
 \ pumvisible() ? "\<C-n>" :
 \ neosnippet#expandable_or_jumpable() ?
 \    "\<Plug>(neosnippet_expand_or_jump)" : "\<TAB>"
smap <expr><TAB> neosnippet#expandable_or_jumpable() ?
\ "\<Plug>(neosnippet_expand_or_jump)" : "\<TAB>"

" For conceal markers.
if has('conceal')
  set conceallevel=2 concealcursor=niv
endif
" }}}

" Tools config --------------------------------------- {{{
"" Status bar
set laststatus=2
set statusline=%F%m%r%h%w%=(%{&ff}/%Y)\ (line\ %l\/%L,\ col\ %c)\

if exists("*fugitive#statusline")
  set statusline+=%{fugitive#statusline()}
endif


"" vim-airline
"let g:airline_theme = 'jellybeans'
let g:airline_theme =  'gruvbox'
let g:airline#extensions#syntastic#enabled = 1
let g:airline#extensions#branch#enabled = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#virtualenv#enabled = 1
let g:airline#extensions#tagbar#enabled = 1

if !exists('g:airline_symbols')
  let g:airline_symbols = {}
endif

if !exists('g:airline_powerline_fonts')
  let g:airline#extensions#tabline#left_sep = ' '
  let g:airline#extensions#tabline#left_alt_sep = '|'
  let g:airline_left_sep          = '▶'
  let g:airline_left_alt_sep      = '»'
  let g:airline_right_sep         = '◀'
  let g:airline_right_alt_sep     = '«'
  let g:airline#extensions#branch#prefix     = '⤴' "➔, ➥, ⎇
  let g:airline#extensions#readonly#symbol   = '⊘'
  let g:airline#extensions#linecolumn#prefix = '¶'
  let g:airline#extensions#paste#symbol      = 'ρ'
  let g:airline_symbols.linenr    = '␊'
  let g:airline_symbols.branch    = '⎇'
  let g:airline_symbols.paste     = 'ρ'
  let g:airline_symbols.paste     = 'Þ'
  let g:airline_symbols.paste     = '∥'
  let g:airline_symbols.whitespace = 'Ξ'
else
  let g:airline#extensions#tabline#left_sep = ''
  let g:airline#extensions#tabline#left_alt_sep = ''

  "" powerline symbols
  let g:airline_left_sep = ''
  let g:airline_left_alt_sep = ''
  let g:airline_right_sep = ''
  let g:airline_right_alt_sep = ''
  let g:airline_symbols.branch = ''
  let g:airline_symbols.readonly = ''
  let g:airline_symbols.linenr = ''
endif

"" MRU
let MRU_Max_Entries = 25

"" Better whitespace
autocmd BufWritePre * StripWhitespace
let g:better_whitespace_verbosity=1

"" Tagbar
let g:tagbar_autofocus = 1

let g:tagbar_type_go = {
      \ 'ctagstype' : 'go',
      \ 'kinds'     : [  'p:package', 'i:imports:1', 'c:constants', 'v:variables',
      \ 't:types',  'n:interfaces', 'w:fields', 'e:embedded', 'm:methods',
      \ 'r:constructor', 'f:functions' ],
      \ 'sro' : '.',
      \ 'kind2scope' : { 't' : 'ctype', 'n' : 'ntype' },
      \ 'scope2kind' : { 'ctype' : 't', 'ntype' : 'n' },
      \ 'ctagsbin'  : 'gotags',
      \ 'ctagsargs' : '-sort -silent'
      \ }
let g:tagbar_type_ruby = {
      \ 'kinds' : [
      \ 'm:modules',
      \ 'c:classes',
      \ 'd:describes',
      \ 'C:contexts',
      \ 'f:methods',
      \ 'F:singleton methods'
      \ ]
      \ }


"" Go
augroup FileType go
  au!
  au FileType go nmap gd <Plug>(go-def)
  au FileType go nmap <Leader>dd <Plug>(go-def-vertical)

  au FileType go nmap <Leader>dv <Plug>(go-doc-vertical)
  au FileType go nmap <Leader>db <Plug>(go-doc-browser)

  au FileType go nmap <Leader>gi <Plug>(go-info)

  au FileType go nmap <leader>gr <Plug>(go-run)
  au FileType go nmap <leader>rb <Plug>(go-build)
  au FileType go nmap <leader>gt <Plug>(go-test)
augroup END

"" The silver searcher configuration
let g:ag_working_path_mode="r"

"" Syntastic
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:javascript_enable_domhtmlcss = 1
let g:syntastic_python_checkers=['python', 'flake8']
let g:syntastic_python_flake8_post_args='--ignore=W391'
let g:syntastic_json_checkers=['jsonlint']
"let g:syntastic_java_javac_config_file_enabled = 1
"let g:syntastic_java_javac_config_file = '.syntastic-classpath'

"" Tagbar configuration
let g:tagbar_autofocus = 1

"" EasyMotion
let g:EasyMotion_smartcase = 1
let g:EasyMotion_prompt = '{n}> '
let g:EasyMotion_use_smartsign_us = 1
let g:EasyMotion_enter_jump_first = 1
let g:EasyMotion_space_jump_first = 1

map  / <Plug>(easymotion-sn)
omap / <Plug>(easymotion-tn)
map  ? <Plug>(easymotion-sn)
omap ? <Plug>(easymotion-sn)
map  n <Plug>(easymotion-next)
map  N <Plug>(easymotion-prev)

"" Ruby plugin configuration
let g:rubycomplete_buffer_loading = 1
let g:rubycomplete_classes_in_global = 1
let g:rubycomplete_rails = 1

augroup vimrc-ruby
  autocmd!
  autocmd BufNewFile,BufRead *.rb,*.rbw,*.gemspec setlocal filetype=ruby
  autocmd FileType ruby set tabstop=2|set shiftwidth=2|set expandtab
augroup END

"" Python plugin configuration
augroup vimrc-python
  autocmd!
  autocmd FileType python setlocal expandtab shiftwidth=4 tabstop=8 colorcolumn=79
        \ formatoptions+=croq softtabstop=4 smartindent
        \ cinwords=if,elif,else,for,while,try,except,finally,def,class,with
augroup END

"" CtrlP settings
let g:ctrlp_match_window = 'bottom,order:ttb'
let g:ctrlp_switch_buffer = 0
let g:ctrlp_working_path_mode = 'ra'
let g:ctrlp_user_command = 'ag %s -l --nocolor --hidden -g ""'

"" NerdTree configurations
let NERDTreeShowBookmarks=1
let NERDTreeIgnore=['\.pyc', '\~$', '\.swo$', '\.swp$', '\.git', '\.hg', '\.svn', '\.bzr']
let NERDTreeChDirMode=0
let NERDTreeQuitOnOpen=1
let NERDTreeMouseMode=2
let NERDTreeShowHidden=1
let NERDTreeKeepTreeInNewTab=1
let g:nerdtree_tabs_open_on_gui_startup=0

"" SnipMate configuration
let g:snipMate = {}
let g:snipMate.scope_aliases = {}
let g:snipMate.scope_aliases['ruby'] = 'ruby,rails'

"" Rainbow parentheses
let g:rbpt_colorpairs = [
      \ ['brown',       'RoyalBlue3'],
      \ ['Darkblue',    'SeaGreen3'],
      \ ['darkgray',    'DarkOrchid3'],
      \ ['darkgreen',   'firebrick3'],
      \ ['darkcyan',    'RoyalBlue3'],
      \ ['darkred',     'SeaGreen3'],
      \ ['darkmagenta', 'DarkOrchid3'],
      \ ['brown',       'firebrick3'],
      \ ['gray',        'RoyalBlue3'],
      \ ['black',       'SeaGreen3'],
      \ ['darkmagenta', 'DarkOrchid3'],
      \ ['Darkblue',    'firebrick3'],
      \ ['darkgreen',   'RoyalBlue3'],
      \ ['darkcyan',    'SeaGreen3'],
      \ ['darkred',     'DarkOrchid3'],
      \ ['red',         'firebrick3'],
      \ ]
let g:rbpt_max = 16
let g:rbpt_loadcmd_toggle = 0
au VimEnter * RainbowParenthesesToggle
au Syntax * RainbowParenthesesLoadRound
au Syntax * RainbowParenthesesLoadSquare
au Syntax * RainbowParenthesesLoadBraces

" Deoplet
let g:deoplete#enable_at_startup = 1

"" Last configurations - after all steps

" it didn't exist before Vim v7.3, sometimes I encounter older versions of vim (centOS, looking at you!!)
if v:version >= 703
  " a faint grey (gray?) color, not too insistent
  highlight ColorColumn term=reverse ctermbg=233 guibg=#202020
  " put the marker(s) at 'textwidth+2' (and at position 120)
  set colorcolumn=120,150
  " if we're called as '*view', or on a console, turn off the colorcolumn
  if v:progname =~? 'view' || &term =~? 'linux|console'
    set colorcolumn=
  endif
endif
" }}}

" Fold, gets it's own section  ----------------------- {{{

function! MyFoldText()
    let line = getline(v:foldstart)

    let nucolwidth = &fdc + &number * &numberwidth
    let windowwidth = winwidth(0) - nucolwidth - 3
    let foldedlinecount = v:foldend - v:foldstart

    " expand tabs into spaces
    let onetab = strpart('          ', 0, &tabstop)
    let line = substitute(line, '\t', onetab, 'g')

    let line = strpart(line, 0, windowwidth - 2 -len(foldedlinecount))
    let fillcharcount = windowwidth - len(line) - len(foldedlinecount)
    return line . '…' . repeat(" ",fillcharcount) . foldedlinecount . '…' . ' '
endfunction



set foldtext=MyFoldText()

autocmd InsertEnter * if !exists('w:last_fdm') | let w:last_fdm=&foldmethod | setlocal foldmethod=manual | endif
autocmd InsertLeave,WinLeave * if exists('w:last_fdm') | let &l:foldmethod=w:last_fdm | unlet w:last_fdm | endif

autocmd FileType vim setlocal fdc=1
set foldlevel=99
" Space to toggle folds.
nnoremap <Space> za
vnoremap <Space> za
autocmd FileType vim setlocal foldmethod=marker
autocmd FileType vim setlocal foldlevel=0
" }}}
