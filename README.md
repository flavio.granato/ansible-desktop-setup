ansible-desktop-setup
====================

Personal project to setup machine after HD format

# Local launch command from virgin PC :
    wget -O- https://raw.githubusercontent.com/jdauphant/ansible-ubuntu-desktop/master/setup.sh | sh
# Local launch if after clone the repository
    git submodule init
    git submodule update
    ansible-playbook installation.yml --sudo -K -c local -i "localhost,"



This project is a copy of https://github.com/jdauphant/ansible-ubuntu-desktop repo
