" Setup language -------------------------------------------------------  {{{
language en_US.UTF-8           " Solve some plugins incompatibilities
" }}}"

" Setup dein {{{
if &compatible
  set nocompatible
endif
filetype off
if (!isdirectory(expand("$HOME/.vim/autoload/plug.vim")))
  call system(expand("mkdir -p $HOME/.vim/autoload"))
  call system(expand("mkdir -p ~/.vim/plugged"))
  call system(expand("git clone https://github.com/junegunn/vim-plug $HOME/.vim/autoload/"))
endif


call plug#begin('~/.vim/plugged')

Plug 'Shougo/vimproc', {'do' : 'make'}
Plug 'Shougo/denite.nvim'
Plug 'Shougo/neomru.vim'

" " Colorschemes {{{
" " Dark themes
" " Improved terminal version of molokai, almost identical to the GUI one
Plug 'joedicastro/vim-molokai256'
Plug 'tomasr/molokai'
Plug 'sjl/badwolf'
Plug 'nielsmadan/harlequin'
Plug 'ajmwagar/vim-dues'
" " Light themes
Plug 'vim-scripts/summerfruit256.vim'
Plug 'joedicastro/vim-github256'
" " Make terminal themes from GUI themes
Plug 'godlygeek/csapprox', {'on' : ['CSApprox', 'CSApproxSnapshot']}
" " }}}


" " Shougo neocomplete ------------------------------------------------------- {{{
Plug 'Shougo/neocomplete.vim'
Plug 'Shougo/neco-vim', {'for': 'vim'}
Plug 'Shougo/neoinclude.vim'
Plug 'ujihisa/neco-look'
" " }}}

" " Aesthetics
" " Themes
Plug 'morhetz/gruvbox'
Plug 'tomasr/molokai'
Plug 'wesgibbs/vim-irblack'
Plug 'frankier/neovim-colors-solarized-truecolor-only'
Plug 'romainl/flattened'
Plug 'nanotech/jellybeans.vim'
Plug 'w0ng/vim-hybrid'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'mhinz/vim-startify'
Plug 'vim-scripts/vim-niji'

" "" Tools
Plug 'MattesGroeger/vim-bookmarks'
Plug 'Raimondi/delimitMate'
Plug 'mbbill/undotree'
Plug 'rking/ag.vim'
Plug 'easymotion/vim-easymotion'
Plug 'majutsushi/tagbar'
Plug 'ntpeters/vim-better-whitespace'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'tacahiroy/ctrlp-funky'
Plug 'scrooloose/nerdtree'
Plug 'terryma/vim-multiple-cursors'
Plug 'dyng/ctrlsf.vim'
Plug 'chrisbra/NrrwRgn'
Plug 'Shougo/neosnippet'
Plug 'Shougo/neosnippet-snippets'
Plug 'majutsushi/tagbar'
Plug 'AndrewRadev/splitjoin.vim'
Plug 'tpope/vim-obsession'
Plug 'jiangmiao/auto-pairs'

" """ Git tools
Plug 'tpope/vim-fugitive'
Plug 'Kocha/vim-unite-tig'
Plug 'airblade/vim-gitgutter'

" "" Languages

" "" Elixir
Plug 'elixir-lang/vim-elixir', {'for': 'elixir'}

" "" Rust
Plug 'rust-lang/rust.vim', {'for': 'rust'}
Plug 'cespare/vim-toml', {'for': ['toml', 'rust']}

" "" Clojure
Plug 'guns/vim-clojure-static', {'for': 'clojure'}
Plug 'kien/rainbow_parentheses.vim', {'for': 'clojure'}
Plug 'tpope/vim-fireplace', {'for': 'clojure'}
Plug 'venantius/vim-cljfmt', {'for': 'clojure'}
Plug 'clojure-vim/async-clj-omni', {'for': 'clojure'}

" "" Go Lang Bundle
Plug 'fatih/vim-go', {'for': 'go'}

" " Markdown & reStructuredText {{{
Plug 'gabrielelana/vim-markdown', {'for': 'markdown'}
" " }}}

" "" html + css + javascript
Plug 'othree/html5.vim', {'for': ['html', 'xhttml', 'css']}
Plug 'mattn/emmet-vim',{'for': ['html', 'xhtml', 'css', 'xml', 'xls', 'markdown']}
Plug 'kchmck/vim-coffee-script', {'for' : 'coffee'}
Plug 'alvan/vim-closetag'
Plug 'pangloss/vim-javascript', {'for': 'javascript'}
Plug 'othree/javascript-libraries-syntax.vim', {'for': 'javascript'}
Plug 'hail2u/vim-css3-syntax', {'for': 'css'}
Plug 'gorodinskiy/vim-coloresque'
Plug 'skammer/vim-css-color'
Plug 'tpope/vim-haml', {'for': 'haml'}
Plug 'elzr/vim-json', {'for': ['json', 'javascript']}
" " Mustache/handlebars
Plug 'mustache/vim-mustache-handlebars'

" "" others tools for langs
Plug 'tpope/vim-commentary'
Plug 'neomake/neomake'
Plug 'dojoteef/neomake-autolint'
Plug 'chrisbra/csv.vim', {'for': 'csv'}
Plug 'tpope/vim-endwise'
Plug 'pearofducks/ansible-vim', {'for': 'yaml'}
Plug 'tfnico/vim-gradle', {'for': 'groovy'}
Plug 'kien/rainbow_parentheses.vim'

" "" SnipMate
Plug 'MarcWeber/vim-addon-mw-utils'
Plug 'tomtom/tlib_vim'
Plug 'garbas/vim-snipmate'
Plug 'honza/vim-snippets'

" " these need to be added last
Plug 'ryanoasis/vim-devicons'
Plug 'tiagofumo/vim-nerdtree-syntax-highlight'
Plug 'carlitux/denite-cool-grep'
Plug 'Yabes/vim-complete-commit-type'

call plug#end()

filetype plugin indent on
" }}}

" Init ----------------------------------------------- {{{

set noexrc
set background=dark
set fenc=utf-8
set cpoptions=aABceFsmq
"             |||||||||
"             ||||||||+-- When joining lines, leave the cursor between joined lines
"             |||||||+-- When a new match is created (showmatch) pause for .5
"             ||||||+-- Set buffer options when entering the buffer
"             |||||+-- :write command updates current file name automatically add <CR> to the last line when using :@r
"             |||+-- Searching continues at the end of the match at the cursor position
"             ||+-- A backslash has no special meaning in mappings
"             |+-- :write updates alternative file name
"             +-- :read updates alternative file name
let g:skip_loading_mswin=1 " Just in case :)

" Newish
set history=9999 " big old history
set timeoutlen=300 " super low delay (works for me)
set formatoptions+=n " Recognize numbered lists
set formatlistpat=^\\s*\\(\\d\\\|[-*]\\)\\+[\\]:.)}\\t\ ]\\s* "and bullets, too
set viminfo+=! " Store upper-case registers in viminfo
set nomore " Short nomore

" }}}

" Settings ------------------------------------------- {{{

syntax enable
set termguicolors
set background=dark
color hybrid "gruvbox

let g:hybrid_custom_term_colors = 1
let g:hybrid_reduced_contrast = 1 " Remove this line if using the default palette."

"let g:solarized_termcolors=256
"let g:solarized_termtrans=0

set regexpengine=1

set encoding=utf-8
set fileencoding=utf-8
set fileencodings=utf-8
filetype indent on

set directory=/tmp/
set smarttab
set shiftwidth=2
set shiftround
set softtabstop=0
set expandtab

syntax on
set wmnu
set wildmode=list:longest,list:full
set list " Show invisible characters


set listchars=""
set listchars=tab:▸▸
set listchars+=trail:.
set listchars+=extends:>
set listchars+=precedes:<
set visualbell
set noerrorbells

set hidden                                                " don't unload buffer when switching away
set modeline                                              " allow per-file settings via modeline
set secure                                                " disable unsafe commands in local .vimrc files
set nobackup nowritebackup noswapfile autoread            " no backup or swap
set hlsearch incsearch ignorecase smartcase               " search
set wildmenu                                              " completion
set backspace=indent,eol,start                            " sane backspace
set clipboard=unnamedplus                                 " use the system clipboard for yank/put/delete
set mouse=a                                               " enable mouse for all modes settings
set nomousehide                                           " don't hide the mouse cursor while typing
set mousemodel=popup                                      " right-click pops up context menu
set ruler                                                 " show cursor position in status bar
set number                                                " show absolute line number of the current line
set relativenumber
set nofoldenable                                          " I fucking hate code folding
set scrolloff=10                                          " scroll the window so we can always see 10 lines around the cursor
set textwidth=80                                          " wrap at 80 characters like a valid human
set printoptions=paper:letter                             " use letter as the print output format
set winaltkeys=no                                         " turn off stupid fucking alt shortcuts
set laststatus=2                                          " always show status bar
set shell=/bin/sh
set bomb
set binary
set ttyfast
set fileformats=unix,dos,mac
set showcmd
set cursorline                                            " Highlight current line
set complete-=i                                           " set complete=.,w,b,u,t
set completeopt=menu,preview
set tags+=.git/tags
set nojoinspaces                                          " Don't add 2 spaces when using J
set autowriteall                                          " Write the contents of the file as frequent as possible

function! ToggleGUICruft()
  if &guioptions=='i'
    exec('set guioptions=imTrL')
  else
    exec('set guioptions=i')
  endif
endfunction

map <F11> <Esc>:call ToggleGUICruft()<cr>

" by default, hide gui menus
set guioptions=i

" }}}

" Autocommands --------------------------------------- {{{
"" Json file
au BufRead,BufNewFile *.json set filetype=json

"" markdown configuration
autocmd BufNewFile,BufReadPost *.md set filetype=markdown " for plugin understand all files with extension .md not only README.md

"" clojurescript configuration
autocmd BufRead,BufNewFile *.cljs setlocal filetype=clojure

"" XML file
au FileType xml setlocal equalprg=xmllint\ --format\ --recover\ -\ 2>/dev/null
" }}}

" Mappings ------------------------------------------- {{{
"" Map leader to ,
let mapleader=','
let maplocalleader= ' '

" Move selected block up/down in visual mode
vnoremap J :m '>+1<CR>gv=gv
vnoremap K :m '<-2<CR>gv=gv

"" Tabs
nnoremap <Tab> gt
nnoremap <S-Tab> gT
nnoremap <silent> <S-t> :tabnew<CR>

"" Set working directory
nnoremap <leader>. :lcd %:p:h<CR>

"" Opens an edit command with the path of the currently edited file filled in
noremap <Leader>e :e <C-R>=expand("%:p:h") . "/" <CR>

"" Opens a tab edit command with the path of the currently edited file filled
noremap <Leader>te :tabe <C-R>=expand("%:p:h") . "/" <CR>

" Format entire file
nmap <leader>fef ggVG=

"" Copy and paste
noremap YY "+y<CR>
noremap P "+gP<CR>
noremap XX "+x<CR>


"" Buffer nav
noremap <leader>z :bp<CR>
noremap <leader>q :bp<CR>
noremap <leader>x :bn<CR>
noremap <leader>w :bn<CR>

"" Close buffer
noremap <leader>c :bd<CR>

"" Clean search (highlight)
nnoremap <silent> <leader><space> :noh<cr>

"" Key binding
"" NerdTree
map <C-e> :NERDTreeToggle<CR>:NERDTreeMirror<CR>
map <leader>e :NERDTreeFind<CR>
nmap <leader>nt :NERDTreeFind<CR>

"" LESS
nnoremap <Leader>m :w <BAR> !lessc % > %:t:r.css<CR><space>

"" JSON
nmap <leader>jt <Esc>:%!python -m json.tool<CR><Esc>:set filetype=json<CR>

"" Fix tab, change tab by spaces
nmap <leader>rt <Esc>:set softtabstop=0<CR><Esc>:set expandtab<CR><Esc>:set smarttab<CR><Esc>:set shiftwidth=2<CR>gg=G<Esc>:retab<CR>

"" HTML identation
nmap <leader>ht <Esc>:!tidy -mi -html -wrap 0 %<CR><Esc>:set filetype=html<CR>

"" Fugitive
nnoremap <silent> <leader>gs :Gstatus<CR>
nnoremap <silent> <leader>gd :Gdiff<CR>
nnoremap <silent> <leader>gc :Gcommit<CR>
nnoremap <silent> <leader>gb :Gblame<CR>
nnoremap <silent> <leader>gl :Glog<CR>
nnoremap <silent> <leader>gp :Git push<CR>
"" Mnemonic_i_nteractive
nnoremap <silent> <leader>gi :Git add -p %<CR>
nnoremap <silent> <leader>gg :SignifyToggle<CR>

"" Tagbar
nmap <silent> <F4> :TagbarToggle<CR>

"" RSpec
map <Leader>t :call RunCurrentSpecFile()<CR>
map <Leader>s :call RunNearestSpec()<CR>
map <Leader>l :call RunLastSpec()<CR>
map <Leader>a :call RunAllSpecs()<CR>

"" Ruby Refactoring
nnoremap <leader>rap  :RAddParameter<cr>
nnoremap <leader>rcpc :RConvertPostConditional<cr>
nnoremap <leader>rel  :RExtractLet<cr>
vnoremap <leader>rec  :RExtractConstant<cr>
vnoremap <leader>relv :RExtractLocalVariable<cr>
nnoremap <leader>rit  :RInlineTemp<cr>
vnoremap <leader>rrlv :RRenameLocalVariable<cr>
vnoremap <leader>rriv :RRenameInstanceVariable<cr>
vnoremap <leader>rem  :RExtractMethod<cr>

"" Unite tig
nnoremap <silent> <leader>ut :<C-u>Unite tig -no-split<CR>
nnoremap <silent> <leader>uta :<C-u>Unite tig -no-split -auto-preview<CR>

"" Undotree
nnoremap <silent> <leader>u :UndotreeToggle<CR>

"" CtrlPFunky
nnoremap <Leader>fu :CtrlPFunky<Cr>
" narrow the list down with a word under cursor
nnoremap <Leader>fU :execute 'CtrlPFunky ' . expand('<cword>')<Cr>

"" CtrlSF
nmap     <C-F>f <Plug>CtrlSFPrompt
vmap     <C-F>f <Plug>CtrlSFVwordPath
vmap     <C-F>F <Plug>CtrlSFVwordExec
nmap     <C-F>n <Plug>CtrlSFCwordPath
nmap     <C-F>p <Plug>CtrlSFPwordPath
nnoremap <C-F>o :CtrlSFOpen<CR>
nnoremap <C-F>t :CtrlSFToggle<CR>
inoremap <C-F>t <Esc>:CtrlSFToggle<CR>

" Plugin key-mappings.
imap <C-k>     <Plug>(neosnippet_expand_or_jump)
smap <C-k>     <Plug>(neosnippet_expand_or_jump)
xmap <C-k>     <Plug>(neosnippet_expand_target)

" SuperTab like snippets behavior.
imap <expr><TAB>
 \ pumvisible() ? "\<C-n>" :
 \ neosnippet#expandable_or_jumpable() ?
 \    "\<Plug>(neosnippet_expand_or_jump)" : "\<TAB>"
smap <expr><TAB> neosnippet#expandable_or_jumpable() ?
\ "\<Plug>(neosnippet_expand_or_jump)" : "\<TAB>"

" For conceal markers.
if has('conceal')
  set conceallevel=2 concealcursor=niv
endif
" }}}

" Tools config --------------------------------------- {{{
"" Status bar
set laststatus=2
set statusline=%F%m%r%h%w%=(%{&ff}/%Y)\ (line\ %l\/%L,\ col\ %c)\

if exists("*fugitive#statusline")
  set statusline+=%{fugitive#statusline()}
endif


"" vim-airline
"let g:airline_theme = 'jellybeans'
let g:airline_theme =  'hybrid'
let g:airline#extensions#syntastic#enabled = 1
let g:airline#extensions#branch#enabled = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#virtualenv#enabled = 1
let g:airline#extensions#tagbar#enabled = 1

if !exists('g:airline_symbols')
  let g:airline_symbols = {}
endif

if !exists('g:airline_powerline_fonts')
  let g:airline#extensions#tabline#left_sep = ' '
  let g:airline#extensions#tabline#left_alt_sep = '|'
  let g:airline_left_sep          = '▶'
  let g:airline_left_alt_sep      = '»'
  let g:airline_right_sep         = '◀'
  let g:airline_right_alt_sep     = '«'
  let g:airline#extensions#branch#prefix     = '⤴' "➔, ➥, ⎇
  let g:airline#extensions#readonly#symbol   = '⊘'
  let g:airline#extensions#linecolumn#prefix = '¶'
  let g:airline#extensions#paste#symbol      = 'ρ'
  let g:airline_symbols.linenr    = '␊'
  let g:airline_symbols.branch    = '⎇'
  let g:airline_symbols.paste     = 'ρ'
  let g:airline_symbols.paste     = 'Þ'
  let g:airline_symbols.paste     = '∥'
  let g:airline_symbols.whitespace = 'Ξ'
else
  let g:airline#extensions#tabline#left_sep = ''
  let g:airline#extensions#tabline#left_alt_sep = ''

  "" powerline symbols
  let g:airline_left_sep = ''
  let g:airline_left_alt_sep = ''
  let g:airline_right_sep = ''
  let g:airline_right_alt_sep = ''
  let g:airline_symbols.branch = ''
  let g:airline_symbols.readonly = ''
  let g:airline_symbols.linenr = ''
endif

"" MRU
let MRU_Max_Entries = 25

"" Better whitespace
autocmd BufWritePre * StripWhitespace
let g:better_whitespace_verbosity=1

"" Tagbar
let g:tagbar_autofocus = 1

let g:tagbar_type_go = {
      \ 'ctagstype' : 'go',
      \ 'kinds'     : [  'p:package', 'i:imports:1', 'c:constants', 'v:variables',
      \ 't:types',  'n:interfaces', 'w:fields', 'e:embedded', 'm:methods',
      \ 'r:constructor', 'f:functions' ],
      \ 'sro' : '.',
      \ 'kind2scope' : { 't' : 'ctype', 'n' : 'ntype' },
      \ 'scope2kind' : { 'ctype' : 't', 'ntype' : 'n' },
      \ 'ctagsbin'  : 'gotags',
      \ 'ctagsargs' : '-sort -silent'
      \ }
let g:tagbar_type_ruby = {
      \ 'kinds' : [
      \ 'm:modules',
      \ 'c:classes',
      \ 'd:describes',
      \ 'C:contexts',
      \ 'f:methods',
      \ 'F:singleton methods'
      \ ]
      \ }


"" Go
augroup FileType go
  au!
  au FileType go nmap gd <Plug>(go-def)
  au FileType go nmap <Leader>dd <Plug>(go-def-vertical)

  au FileType go nmap <Leader>dv <Plug>(go-doc-vertical)
  au FileType go nmap <Leader>db <Plug>(go-doc-browser)

  au FileType go nmap <Leader>gi <Plug>(go-info)

  au FileType go nmap <leader>gr <Plug>(go-run)
  au FileType go nmap <leader>rb <Plug>(go-build)
  au FileType go nmap <leader>gt <Plug>(go-test)
augroup END

"" The silver searcher configuration
let g:ag_working_path_mode="r"

"" Syntastic
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:javascript_enable_domhtmlcss = 1
let g:syntastic_python_checkers=['python', 'flake8']
let g:syntastic_python_flake8_post_args='--ignore=W391'
let g:syntastic_json_checkers=['jsonlint']
"let g:syntastic_java_javac_config_file_enabled = 1
"let g:syntastic_java_javac_config_file = '.syntastic-classpath'

"" Tagbar configuration
let g:tagbar_autofocus = 1

"" EasyMotion
let g:EasyMotion_smartcase = 1
let g:EasyMotion_prompt = '{n}> '
let g:EasyMotion_use_smartsign_us = 1
let g:EasyMotion_enter_jump_first = 1
let g:EasyMotion_space_jump_first = 1

map  / <Plug>(easymotion-sn)
omap / <Plug>(easymotion-tn)
map  ? <Plug>(easymotion-sn)
omap ? <Plug>(easymotion-sn)
map  n <Plug>(easymotion-next)
map  N <Plug>(easymotion-prev)

"" Ruby plugin configuration
let g:rubycomplete_buffer_loading = 1
let g:rubycomplete_classes_in_global = 1
let g:rubycomplete_rails = 1

augroup vimrc-ruby
  autocmd!
  autocmd BufNewFile,BufRead *.rb,*.rbw,*.gemspec setlocal filetype=ruby
  autocmd FileType ruby set tabstop=2|set shiftwidth=2|set expandtab
augroup END

"" Python plugin configuration
augroup vimrc-python
  autocmd!
  autocmd FileType python setlocal expandtab shiftwidth=4 tabstop=8 colorcolumn=79
        \ formatoptions+=croq softtabstop=4 smartindent
        \ cinwords=if,elif,else,for,while,try,except,finally,def,class,with
augroup END

"" CtrlP settings
let g:ctrlp_match_window = 'bottom,order:ttb'
let g:ctrlp_switch_buffer = 0
let g:ctrlp_working_path_mode = 'ra'
let g:ctrlp_user_command = 'ag %s -l --nocolor --hidden -g ""'

"" NerdTree configurations
let NERDTreeShowBookmarks=1
let NERDTreeIgnore=['\.pyc', '\~$', '\.swo$', '\.swp$', '\.git', '\.hg', '\.svn', '\.bzr']
let NERDTreeChDirMode=0
let NERDTreeQuitOnOpen=1
let NERDTreeMouseMode=2
let NERDTreeShowHidden=1
let NERDTreeKeepTreeInNewTab=1
let g:nerdtree_tabs_open_on_gui_startup=0

"" SnipMate configuration
let g:snipMate = {}
let g:snipMate.scope_aliases = {}
let g:snipMate.scope_aliases['ruby'] = 'ruby,rails'

"" Rainbow parentheses
let g:rbpt_colorpairs = [
      \ ['brown',       'RoyalBlue3'],
      \ ['Darkblue',    'SeaGreen3'],
      \ ['darkgray',    'DarkOrchid3'],
      \ ['darkgreen',   'firebrick3'],
      \ ['darkcyan',    'RoyalBlue3'],
      \ ['darkred',     'SeaGreen3'],
      \ ['darkmagenta', 'DarkOrchid3'],
      \ ['brown',       'firebrick3'],
      \ ['gray',        'RoyalBlue3'],
      \ ['black',       'SeaGreen3'],
      \ ['darkmagenta', 'DarkOrchid3'],
      \ ['Darkblue',    'firebrick3'],
      \ ['darkgreen',   'RoyalBlue3'],
      \ ['darkcyan',    'SeaGreen3'],
      \ ['darkred',     'DarkOrchid3'],
      \ ['red',         'firebrick3'],
      \ ]
let g:rbpt_max = 16
let g:rbpt_loadcmd_toggle = 0
au VimEnter * RainbowParenthesesToggle
au Syntax * RainbowParenthesesLoadRound
au Syntax * RainbowParenthesesLoadSquare
au Syntax * RainbowParenthesesLoadBraces

" Neocomplete
let g:neocomplete#enable_at_startup = 1
" }}}

" Fold, gets it's own section  ----------------------- {{{

function! MyFoldText()
    let line = getline(v:foldstart)

    let nucolwidth = &fdc + &number * &numberwidth
    let windowwidth = winwidth(0) - nucolwidth - 3
    let foldedlinecount = v:foldend - v:foldstart

    " expand tabs into spaces
    let onetab = strpart('          ', 0, &tabstop)
    let line = substitute(line, '\t', onetab, 'g')

    let line = strpart(line, 0, windowwidth - 2 -len(foldedlinecount))
    let fillcharcount = windowwidth - len(line) - len(foldedlinecount)
    return line . '…' . repeat(" ",fillcharcount) . foldedlinecount . '…' . ' '
endfunction



set foldtext=MyFoldText()

autocmd InsertEnter * if !exists('w:last_fdm') | let w:last_fdm=&foldmethod | setlocal foldmethod=manual | endif
autocmd InsertLeave,WinLeave * if exists('w:last_fdm') | let &l:foldmethod=w:last_fdm | unlet w:last_fdm | endif

autocmd FileType vim setlocal fdc=1
set foldlevel=99
" Space to toggle folds.
nnoremap <Space> za
vnoremap <Space> za
autocmd FileType vim setlocal foldmethod=marker
autocmd FileType vim setlocal foldlevel=0
" }}}
