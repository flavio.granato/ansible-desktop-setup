build:
	docker build -t fgranato/dotfiles .

build-nc:
	docker build --no-cache -t fgranato/dotfiles .

run:
	docker run -d fgranato/dotfiles
